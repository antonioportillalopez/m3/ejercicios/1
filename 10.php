<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // Inicilización de variables
        $rad=0;
        $resultado=0.0;
        define ("PI", pi());// se define la constante pi con el valor que da la funcion.
                        
        // asignación de variables
        $rad=$_GET["rad"];
        
        // operaciones reutilizando la variable resultado
        echo "<div>Según el radio $rad de la circunferencia</div>";
                
        $resultado=2*PI*$rad;
        echo "<div>su lóngitud es  $resultado</div>";
        $resultado=PI*pow($rad,2);
        echo "<div>su área es  $resultado</div>";
        $resultado=$resultado*4; // se reutiliza el cálculo anterior
        echo "<div>su área si fuese una esfera es  $resultado</div>";
        ?>
    </body>
</html>
