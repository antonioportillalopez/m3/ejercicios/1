<!DOCTYPE html>
<!--
Programa que lea una variable y diga si es a o b
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $caracter="";
        $caracter="c";
        if($caracter=="a"){
            echo 'El carácter es "a"';
            
        } else  if ($caracter=="b"){
            echo 'El carácter es "b"';
        } else {
            echo 'El carácter no es ni "a" ni "b"';
        }
        ?>
    </body>
</html>
