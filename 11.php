<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // inicializamos variables
        $resultado="";
        $n=0.0;
        
        // asignación de variables
        $n=$_REQUEST["nota"];
        
        
        // operaciones reutilizando la variable resultado
        echo "LA NOTA $n CORREPONDE A ";
        if ($n>=0&$n<3)  {
            echo "MUY DEFICIENTE";
        } elseif ($n>=3&$n<5) {
            echo "SUPENSO";
        } elseif ($n>=5&$n<6){
            echo "SUFICIENTE";
        } elseif ($n>=6&$n<7){
            echo "BIEN";
        } elseif ($n>=7&$n<9){
            echo "NOTABLE";
        } elseif ($n>=9&$n<=10){
            echo "SOBRESALIENTE";
        } else {
            echo "UNA NOTA FUERA DE RANGO (1 A 10)";
        }
        ?>
    </body>
</html>
