<!DOCTYPE html>
<html>
    <head>
        <!-- Se definen varios estilos -->
        <style> 
            .uno{
                  background-color: black;
                  color: white;
                  margin: 20px;
                  padding: 20px;
            }
            .dos{
                  background-color: black;
                  color: yellowgreen;
                  margin: 12px;
                  padding: 9px;
            }
            .tres{
                  text-align: center;
                  background-color: graytext;
                  color:#800000;
                  font-size:14px;
                  font-family:georgia,serif;
                  margin: 20px;
                  padding: 20px;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="uno"><?=$_POST["campo1"]?></div>
        <div class="dos"><?=$_POST["campo2"]?></div>
        <div class="tres"><?=$_POST["campo3"]?></div>
        <div style="color: blue;"><strong><?=$_POST["campo4"]?></strong></div> <!-- Se aplica el estilo directamente sin utilizar nombres de class predefinidos-->
    </body>
</html>
