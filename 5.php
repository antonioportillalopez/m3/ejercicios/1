<!DOCTYPE html>
<!--
indica si un número es par o impar (se sobreentiende que es un número entero y no real)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $resto=0;
        $numero=-78778;
        
        $resto=fmod($numero,2); // para la funcion fmod que calcula el resto es posible poner cualquier número real
        if ($resto==0){
            echo 'El número es par';
        } else{
            echo 'El número es impar';
        }
        ?>
    </body>
</html>