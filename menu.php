<!DOCTYPE html> <!-- esta etiqueta dice que es HTML5 -->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style> /*aspectos visuales */
            *{ /* selector */

                margin:0px;
                padding:0xp;
            }
            html,body{
                width: 100%; 
                height: 100%;
            }
        </style>

        <script language="javascript"> /* elementos de programacion  , languge="javascript" es la opcion por defecto si no se pone nada*/
            /* aqui va el código javascript
             a = prompt("in"); // java script -> derivados que funcionan en el navegador: ecma script lo incorparan algunos, type script necesita un interprete  que lo pasará a java script
             */
        </script>
    </head>
    <body>
        <?php $ejercicios=[
            "1.php",
            "2.html",
            "3.php",
            "4.php",
            "5.php",
            "6.php",
            "7.php",
            "8.php",
            "9.html",
            "10.htm",
            "11.htm",
            "12.htm",
            ] ?>
        
        <div>
            <ul> <!-- creación de menús-->
                <?php 
                 foreach ($ejercicios as $num=>$contenido) {
                     $ejercicio=$num+1;
                 echo '<li><a href="'.$contenido.'"> Parte '.$ejercicio.'</a></li>';
                 }
                ?>
            </ul>
        </div>
    </body>
</html>
